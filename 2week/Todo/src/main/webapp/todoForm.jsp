<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Language" content="ko">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Todo Regiser</title>
</head>
<body>
<%
    request.setCharacterEncoding("EUC-KR");
%>

${requestScope.forward}
<br>

<h2>할일 등록</h2>

<form action="todoAdd" method="GET">
	<h4>어떤 일인가요?</h4><br>
	<input type="text" name="title"><br>
	<h4>누가 할일인가요?</h4><br>
	<input type="text" name="name">
	<h4>우선 순위를 선택하세요.</h4>
	<input type="radio" name="sequence" value="1">1순위<br>
	<input type="radio" name="sequence" value="2">2순위<br>
	<input type="radio" name="sequence" value="3">3순위<br>
<input type="submit">
</form>

</body>
</html>