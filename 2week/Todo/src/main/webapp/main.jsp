<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Language" content="ko">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="style.css" rel="stylesheet" type="text/css">
<link rel="icon" href="data:;base64,iVBORw0KGgo=">
<title>main</title>
<script src="http://code.jquery.com/jquery-latest.js"></script>
<script>
$(document).ready(function(){	
    $.ajax({
       type:"GET",
       url:"getdata",
       contentType: "application/x-www-form-urlencoded; charset=utf-8",
       datatype:"json",
       success: function(data) {
    	   
    	   console.log(data);
    	   
    	   var todo="<h3>TODO</h3>";
    	   var doing="<h3>DOING</h3>";
    	   var done="<h3>DONE</h3>";
    	   
	       for (var i=0; i<data.length;i++){
	       		if(data[i].type=="TODO"){
	       			todo+="<div id='"+data[i].id+"' class='work'>"
	       			+"<h5>할일:"+data[i].title+"<p>이름:"+data[i].name+"<p>우선순위:"+data[i].sequence+"</p></h5>"
	       			+"<button type='button' id='change' value="+data[i].id+">완료</button>"
	       			+"</div>";		
	       		}
	       		else if(data[i].type="DOING"){
	       			doing+="<div id='"+data[i].id+"' class='work'>"
	       			+"<h5>할일:"+data[i].title+"<p>이름:"+data[i].name+"<p>우선순위:"+data[i].sequence+"</p></h5>"
	       			+"<button type='button' id='change' value="+data[i].id+">완료</button>"
	       			+"</div>";	
	       		}
	       		else if(data[i].type="DON"){ 
	       			done+="<div id='"+data[i].id+"' class='work'>"
	       			+"<h5>할일:"+data[i].title+"<p>이름:"+data[i].name+"<p>우선순위:"+data[i].sequence+"</p></h5>"
	       			+"</div>";	
	       		}
	       }	
	       document.getElementById('one').innerHTML=todo;
	       document.getElementById('two').innerHTML=doing;
	       document.getElementById('three').innerHTML=done;
       },
       error: function(e) {
         console.log(e);
       }			
   });
});
</script>

<script>
$(function() {
    $(document).on("click","#change",function(){
    	alert("클릭");
    	var value = $(this).attr('value');
    	$.ajax({
  	       type:"POST",
  	       url:"getdata",
  	       async: false,
  	       contentType: "application/x-www-form-urlencoded; charset=utf-8",
  	       data:{"value":value},
  	       datatype:"json",
  	       success: function(data) {
  	   			console.log("success");
  	       }
    	});    	
    	
	    $.ajax({
	       type:"GET",
	       url:"getdata",
	       contentType: "application/x-www-form-urlencoded; charset=utf-8",
	       datatype:"json",
	       success: function(data) {
	    	   console.log(data);
	    	   var todo="<h3>TODO</h3>";
	    	   var doing="<h3>DOING</h3>";
	    	   var done="<h3>DONE</h3>";
	    	   
		       for (var i=0; i<data.length;i++){
		       		if(data[i].type=="TODO"){
		       			todo+="<div id='"+data[i].id+"' class='work'>"
		       			+"<h5>할일:"+data[i].title+"<p>이름:"+data[i].name+"<p>우선순위:"+data[i].sequence+"</p></h5>"
		       			+"<button type='button' id='change' value="+data[i].id+">완료</button>"
		       			+"</div>";		
		       		}
		       		else if(data[i].type="DOING"){
		       			doing+="<div id='"+data[i].id+"' class='work'>"
		       			+"<h5>할일:"+data[i].title+"<p>이름:"+data[i].name+"<p>우선순위:"+data[i].sequence+"</p></h5>"
		       			+"<button type='button' id='change' value="+data[i].id+">완료</button>"
		       			+"</div>";	
		       		}
		       		else if(data[i].type="DONE"){ 
		       			done+="<div id='"+data[i].id+"' class='work'>"
		       			+"<h5>할일:"+data[i].title+"<p>이름:"+data[i].name+"<p>우선순위:"+data[i].sequence+"</p></h5>"
		       			+"</div>";	
		       		}
		       }	
		       document.getElementById('one').innerHTML=todo;
		       document.getElementById('two').innerHTML=doing;
		       document.getElementById('three').innerHTML=done;
	       },
	       error: function(e) {
	         console.log(e);
	       }			
	   });
   });  
});


</script>

</head>
<body>
<div id="total">
	<div id="one">
		<h3>TODO</h3>
	</div>
	<div id="two">
		<h3>DOING</h3>
	</div>
	<div id="three">
		<h3>DONE</h3>
	</div>
</div>

<button type="button" onclick="location.href='todoForm.jsp'">일정추가</button>

</body>
</html>