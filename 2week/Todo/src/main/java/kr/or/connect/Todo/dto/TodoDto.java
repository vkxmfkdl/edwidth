package kr.or.connect.Todo.dto;

public class TodoDto {
	private int id;
	private String title;
	private String name;
	private int sequence;
	private String type;
	private String datetime;
	
	public TodoDto() {
		
	}
	
	public TodoDto(int id, String title, String name, int sequence, String type, String datetime) {
		super();
		this.id = id;
		this.title = title;
		this.name = name;
		this.sequence = sequence;
		this.type = type;
		this.datetime = datetime;
	}
	
	public TodoDto(String title, String name, int sequence) {
		super();
		this.title = title;
		this.name = name;
		this.sequence = sequence;
	}


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getSequence() {
		return sequence;
	}

	public void setSequence(int sequence) {
		this.sequence = sequence;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDatetime() {
		return datetime;
	}

	public void setDatetime(String datetime) {
		this.datetime = datetime;
	}

	
	@Override
	public String toString() {
		return "Todo [id="+id+", "
				+ "title="+title+", "
				+ "name="+name+", "
				+ "sequence="+sequence+", "
				+ "type="+type+", "
				+ "datetime="+datetime+"]";
	}	
}
