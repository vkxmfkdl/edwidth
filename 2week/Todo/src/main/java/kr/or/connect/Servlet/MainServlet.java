package kr.or.connect.Servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import kr.or.connect.Todo.dao.TodoDao;
import kr.or.connect.Todo.dto.TodoDto;


@WebServlet("/getdata")
public class MainServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
   
    public MainServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("json/application;charset=UTF-8");

		PrintWriter writer = response.getWriter();
		TodoDao dao = new TodoDao();
		
		JSONArray jsonList = new JSONArray();
		List<TodoDto> todos = dao.getTodos();
		for (TodoDto dto : todos) {
			JSONObject obj = new JSONObject();
			int id = dto.getId();
			String name = dto.getName();
			String title = dto.getTitle();
			String type = dto.getType();
			int sequence = dto.getSequence();
			obj.put("id",id);
			obj.put("name", name);
			obj.put("title", title);
			obj.put("type", type);
			obj.put("sequence",sequence);
			jsonList.add(obj);
		}
		System.out.println(jsonList);
		writer.print(jsonList);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		TodoDao dao = new TodoDao();
		if(request.getParameter("value")!=null) {
			int m_id = Integer.parseInt(request.getParameter("value"));
			//System.out.println("m_id:"+m_id);
			String m_type = dao.getTodo(m_id).getType();
			if(m_type.equals("TODO")) {
				m_type="DOING";
			}
			else if(m_type.equals("DOING")) {
				m_type="DONE";
			}
			int updateCount = dao.updateTodo(m_id, m_type);
		}
	}
}
