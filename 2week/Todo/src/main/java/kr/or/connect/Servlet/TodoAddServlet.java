package kr.or.connect.Servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kr.or.connect.Todo.dao.TodoDao;
import kr.or.connect.Todo.dto.TodoDto;

@WebServlet("/todoAdd")
public class TodoAddServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public TodoAddServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String title = new String(request.getParameter("title").getBytes("8859_1"),"EUC-KR");
		String name = new String(request.getParameter("name").getBytes("8859_1"),"EUC-KR");
		int sequence = Integer.parseInt(request.getParameter("sequence"));
		
		TodoDto Todo = new TodoDto(title,name,sequence);
		TodoDao Dao = new TodoDao();
		
		int insertCount = Dao.addTodo(Todo);
		System.out.println(insertCount);
		
		response.sendRedirect("main.jsp");
	}

}
