package kr.or.connect.Todo.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import kr.or.connect.Todo.dto.TodoDto;

public class TodoDao {

	
	private static String dburl = "jdbc:mysql://localhost:3306/connectdb?serverTimezone=Asia/Seoul&useSSL=false";
	private static String dbUser = "connectuser";
	private static String dbpasswd = "connect123!@#";
	
	public int addTodo(TodoDto Todo) {
		int insertCount = 0;
		
		Connection conn = null;
		PreparedStatement ps =null;
		
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			conn= DriverManager.getConnection(dburl, dbUser, dbpasswd);
			String sql = "INSERT INTO todo (title, name, sequence) VALUES (?,?,?)";
			ps = conn.prepareStatement(sql);
			ps.setString(1, Todo.getTitle());
			ps.setString(2, Todo.getName());
			ps.setInt(3, Todo.getSequence());
			insertCount = ps.executeUpdate();
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			if(ps!=null) {
				try {
					ps.close();
				}catch(Exception e){
					e.printStackTrace();
				}
			}
			if(conn!=null) {
				try {
					conn.close();
				}catch(Exception e){
					e.printStackTrace();
				}
			}
		}
		
		return insertCount;
	}
	
	
	public List<TodoDto> getTodos(){
		List<TodoDto> Todos = new ArrayList<>();
		
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		}catch(Exception e){
			e.printStackTrace();
		}
		
		String sql = "SELECT *FROM todo";
		try(
				Connection conn=DriverManager.getConnection(dburl,dbUser,dbpasswd); 
				PreparedStatement ps = conn.prepareStatement(sql);
		){
			try(ResultSet rs= ps.executeQuery()) {
				while(rs.next()) {
					int id = rs.getInt(1);
					String title = rs.getString(2);
					String name = rs.getString(3);
					int sequence = rs.getInt(4);
					String type = rs.getString(5);
					String datetime = rs.getDate(6).toString();
					TodoDto TodoDto = new TodoDto(id,title,name,sequence,type,datetime);
					System.out.println(TodoDto);
					Todos.add(TodoDto);
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		return Todos;
	}

	public int updateTodo(int id, String type) {
		int updateCount=0;

		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		String sql = "UPDATE todo SET type=? WHERE id=?";
		
		try(
				Connection conn = DriverManager.getConnection(dburl, dbUser, dbpasswd);
				PreparedStatement ps = conn.prepareStatement(sql);
		){
			ps.setString(1,type);
			ps.setInt(2, id);			
			updateCount = ps.executeUpdate();

		}catch(Exception e) {
			e.printStackTrace();
		}
		
		return updateCount;
	}
	
	public TodoDto getTodo(int id) {
		TodoDto dto = null;
		
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		}catch(Exception e){
			e.printStackTrace();
		}
		
		String sql="SELECT * from todo WHERE id=?";
		try(
				Connection conn = DriverManager.getConnection(dburl,dbUser,dbpasswd);
				PreparedStatement ps = conn.prepareStatement(sql);
		) {
			ps.setInt(1, id);
			try(ResultSet rs = ps.executeQuery();) {
				while(rs.next()) {
					int gid = rs.getInt(1);
					String gtitle = rs.getString(2);
					String gname = rs.getString(3);
					int gseqeunce = rs.getInt(4);
					String gtype= rs.getString(5);
					String gdatetime = rs.getDate(6).toString();
					dto = new TodoDto(gid,gtitle, gname, gseqeunce, gtype, gdatetime);
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return dto;
	}
}
